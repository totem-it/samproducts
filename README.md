Sam Products Component
================

This package manage products (books) in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/files/totem.png)

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin)


Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require totem-it/sam-products
```

Usage
-------------

Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)