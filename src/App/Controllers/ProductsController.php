<?php

namespace Totem\SamProducts\App\Controllers;

use App\Http\Controllers\Controller;
use Totem\SamProducts\App\Model\Component;
use Totem\SamProducts\App\Model\Attribute;
use Totem\SamProducts\App\Model\Product;
use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\App\Model\VariantAttribute;
use Totem\SamProducts\App\Repositories\Contracts\ProductRepositoryInterface;
use Totem\SamProducts\Database\Seeds\Products\HardCoverSewnSeeder;
use Totem\SamProducts\Database\Seeds\Products\SoftCoverGluedSeeder;

class ProductsController extends Controller
{
    /**
     * @var ProductRepositoryInterface|\Totem\SamProducts\App\Repositories\ProductRepository
     */
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : \Illuminate\View\View
    {
        $product = Product::where('code', '=', 'hard-cover-sewn')->with([
            'components',
            'components.attributes',
            'components.variants',
            'components.variants.attributes',
            'variants',
            'variants.attributes',
            'attributes',
        ])->first();

        dd($product->toArray());
//        return view('sam-users::users.index')->with([
//            'users' => $this->repository->paginateWithRelations()
//        ]);
    }

    public function calculate(string $slug)
    {
        $product = $this->repository->calculateForm($slug);
//        $product = $this->repository->findByCode($slug);
//
//        (new HardCoverSewnSeeder)->run();
//        (new SoftCoverGluedSeeder())->run();

//dd($product->toArray());
        

//    $options = array_map(function($item){
//        $item['field_type'] =  \Totem\SamProducts\App\Model\Fields\Option::class;
//        return new Attribute($item);
//    }, $options);
//
//dd($options);

//        $product = $this->repository->calculateForm($slug);
//        dump($product);
//        dump($product->toArray());
        return view('theme.product.calculate')->with([
            'product' => $product
        ]);
    }
//
//    public function create(RoleRepositoryInterface $roleRepository) : \Illuminate\View\View
//    {
//        return view('sam-users::users.create')->with([
//            'roles' => $roleRepository->all()
//        ]);
//    }
//
//    public function show(int $id = 0) : \Illuminate\View\View
//    {
//        return view('sam-users::users.show')->with([
//            'user' => $this->repository->findWithRelationsById($id, ['roles']),
//        ]);
//    }
//
//    public function edit(int $id = 0, RoleRepositoryInterface $roleRepository) : \Illuminate\View\View
//    {
//        return view('sam-users::users.edit')->with([
//            'user' => $this->repository->findWithRelationsById($id, ['roles']),
//            'roles' => $roleRepository->all()
//        ]);
//    }
//
//    public function store(UserRequest $request) : \Illuminate\Http\RedirectResponse
//    {
//        $data = $this->repository->store($request);
//
//        return redirect()->route('users.index')->with('success', __('User :name created successfully.', ['name' => $data->fullname]));
//    }
//
//    public function update(UserRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
//    {
//        $data = $this->repository->store($request, $id);
//
//        return redirect()->route('users.index')->with('success', __('User :name updated successfully.', ['name' => $data->fullname]));
//    }
//
//    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
//    {
//        $data = $this->repository->delete($id);
//
//        return redirect()->route('users.index')->with('success', __('User :name deleted successfully.', ['name' => $data->fullname]));
//    }
//
//    public function activate(int $id = 0) : \Illuminate\Http\RedirectResponse
//    {
//        $data = $this->repository->activate($id);
//
//        return redirect()->back()->with('success', __('User :name activated successfully.', ['name' => $data->fullname]));
//    }
//
//    public function deactivate(int $id = 0) : \Illuminate\Http\RedirectResponse
//    {
//        $data = $this->repository->deactivate($id);
//
//        return redirect()->back()->with('success', __('User :name locked successfully.', ['name' => $data->fullname]));
//    }
}
