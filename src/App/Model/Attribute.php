<?php

namespace Totem\SamProducts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property int id
 * @property string code
 * @property string name
 * @property string|null description
 * @property string|null default
 * @property float step
 * @property int min
 * @property int max
 * @property \Illuminate\Support\Collection $attributes
 * @property \Illuminate\Support\Collection $variants
 * @mixin \Eloquent
 */
class Attribute extends Model
{

    use EloquentDecoratorTrait,
        HasUuid;

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'field_type',
            'type',
            'code',
            'name',
            'suffix',
            'description',
            'default',
            'step',
            'min',
            'max',
            'order',
        ]);

        $this->setHidden([
            'created_at',
            'updated_at',
            'deleted_at',
            'type',
            'field_type',
            'pivot',
        ]);

        $this->setAttribute('type', __CLASS__);

        $this->setTable('products_attributes');

        parent::__construct($attributes);

    }

    public function attributes() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(self::class, 'attributable', 'products_attributables', 'attributable_code','attribute_id')->orderBy('order');
    }

    public function variants() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(\Totem\SamProducts\App\Model\Variant::class, 'attributable', 'products_attributables', 'attributable_code','attribute_id')->orderBy('order');
    }

    public function newFromBuilder($attributes = [], $connection = null)
    {
        return $this->decorator('field_type', [], $attributes, $connection);
    }

    protected static function boot() : void
    {
        parent::boot();

        self::globalScope(self::class);
    }

    public static function globalScope(string $class) : void
    {
        static::addGlobalScope('type', function (\Illuminate\Database\Eloquent\Builder $builder) use ($class) {
            $builder->where('type', '=', $class);
        });
    }
}