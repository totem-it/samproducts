<?php

namespace Totem\SamProducts\App\Model;

class CalculationForm
{

    public $id;
    public $code;
    public $name;
    public $description;
    public $keywords;
    public $fields;

    public function __construct(Product $product)
    {
        $this->id           = $product->id;
        $this->code         = $product->code;
        $this->name         = $product->name;
        $this->description  = $product->description;
        $this->keywords     = $product->keywords;

        $this->fields       = $this->makeFields($product);
    }

    private function makeFields($product) : \Illuminate\Support\Collection
    {
        return collect([
            'attributes' => $this->setAttributes($product->attributes, $product->variants),
            'components' => $this->setComponents($product->components),
        ]);
    }

    private function setAttributes(\Illuminate\Support\Collection $attributes, \Illuminate\Support\Collection $variants)
    {
        return $attributes->merge($variants)->sortBy('order');
    }

    private function setComponents(\Illuminate\Support\Collection $components)
    {
        return $components->map(function(Component $item) {
            $item->addHidden([
                'attributes',
                'variants',
            ]);
            $item->fields = $item->attributes->merge($item->variants)->sortBy('order');
            return $item;
        });
    }

}