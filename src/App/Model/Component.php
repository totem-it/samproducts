<?php

namespace Totem\SamProducts\App\Model;

/**
 * @property \Illuminate\Support\Collection fields
 */
class Component extends Attribute
{

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);

        $this->setTable('products_attributes');
        $this->setAttribute('type', __CLASS__);
        $this->addHidden([
            'type',
            'suffix',
            'description',
            'default',
            'step',
            'min',
            'max',
            'order',
        ]);
    }

    public function setFieldsAttribute($value) : void
    {
        $this->attributes['fields'] = $value;
    }

    protected static function boot() : void
    {
        parent::boot();

        self::globalScope(self::class);
    }


}