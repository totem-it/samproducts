<?php

namespace Totem\SamProducts\App\Model;

/**
 * @property \Illuminate\Support\Collection tooltip
 * @property string label
 * @property string for
 */
class Field extends Attribute
{

    public const FOR = 'rel_';
    public const NAME = 'parameter';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->makeVisible([
            'description',
            'default',
            'suffix',
        ]);
        $this->append([
            'label',
            'for',
        ]);
        $this->addHidden([
//           'order',
        ]);
    }

    public function getForAttribute() : string
    {
        return self::FOR . $this->code . substr($this->id, 0, 8);
    }

    public function getLabelAttribute() : string
    {
        return $this->getOriginal('name');
    }

    public function getNameAttribute() : string
    {
        return self::NAME . '['.$this->id.']';
    }

    public function getTooltipAttribute() : \Illuminate\Support\Collection
    {
        return collect($this->description !== null ? [$this->label => $this->description] : []);
    }

    public function getTooltipHtmlAttribute() : string
    {
        return $this->tooltip->map(function(?string $description, string $name){
            return ['title' => '<h4>'.$name.'</h4><p>'.$description.'</p>'];
        })->implode('title', '');
    }
}