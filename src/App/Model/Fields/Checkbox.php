<?php

namespace Totem\SamProducts\App\Model\Fields;

use Totem\SamProducts\App\Model\Field;

class Checkbox extends Field
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->addHidden([
            'suffix',
            'step',
            'min',
            'max',
        ]);
    }


}