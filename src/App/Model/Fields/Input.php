<?php

namespace Totem\SamProducts\App\Model\Fields;

use Totem\SamProducts\App\Model\Field;

/**
 * @property string types
 */
abstract class Input extends Field
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->append([
            'types',
        ]);

    }

    abstract public function getTypesAttribute() : string;

}