<?php

namespace Totem\SamProducts\App\Model\Fields;

class InputNumber extends Input
{

    public function getTypesAttribute(): string
    {
        return 'number';
    }
}