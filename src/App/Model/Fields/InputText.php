<?php

namespace Totem\SamProducts\App\Model\Fields;

class InputText extends Input
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->addHidden([
            'step',
            'min',
            'max',
        ]);
    }

    public function getTypesAttribute(): string
    {
        return 'text';
    }
}