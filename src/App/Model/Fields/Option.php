<?php

namespace Totem\SamProducts\App\Model\Fields;

class Option extends Select
{

    protected $casts = [
        'default' => 'integer'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->append([
            'value',
        ]);
        $this->addHidden([
            'suffix',
            'step',
            'min',
            'max',
        ]);
    }

    public function getNameAttribute() : string
    {
        return $this->getOriginal('name');
    }

    public function getValueAttribute() : string
    {
        return parent::NAME . '['.$this->code.']';
    }
}