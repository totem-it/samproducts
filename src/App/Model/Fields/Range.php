<?php

namespace Totem\SamProducts\App\Model\Fields;

use Totem\SamProducts\App\Model\Field;

/**
 * @property array scale
 */
class Range extends Field
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->addHidden([
            'suffix',
        ]);

        $this->append([
            'scale',
        ]);
    }

    public function getScaleAttribute() : array
    {
        $range = range($this->min, $this->max, $this->step);
        $count = \count($range) - 1;

        return array_map(function ($item, $index) use ($count) {
            return [
                'index' => ($index / $count) * 100,
                'label' => $item
            ];
        }, $range, array_keys($range));
    }
}