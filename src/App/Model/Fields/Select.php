<?php

namespace Totem\SamProducts\App\Model\Fields;

use Totem\SamProducts\App\Model\Field;

class Select extends Field
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->addHidden([
            'step',
            'min',
            'max',
            'default',
        ]);
    }

    public function getTooltipAttribute() : \Illuminate\Support\Collection
    {
        /** @var \Illuminate\Support\Collection $options */
        $select = parent::getTooltipAttribute();
        $options = $this->getRelation('attributes')->filter(function(\Totem\SamProducts\App\Model\Attribute $attribute){
            return $attribute->description !== null;
        })->pluck('description', 'name');

        if ($select->isNotEmpty() && $options->isNotEmpty()) {
            return $select->merge($options);
        }
        if ($select->isNotEmpty()) {
            return $select;
        }
        return $options;
    }

}