<?php

namespace Totem\SamProducts\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamProducts\App\Model\Contracts\ProductInterface;

/**
 * @property int id
 * @property string code
 * @property string name
 * @property string|null description
 * @property string|null keywords
 * @property \Illuminate\Support\Collection components
 * @mixin \Eloquent
 */
class Product extends Model implements ProductInterface
{
    public function __construct(array $attributes = [])
    {
        $this->setHidden([
            'created_at',
            'updated_at',
            'deleted_at',
        ]);

        $this->fillable([
            'code',
            'name',
            'description',
            'keywords',
            'image',
            'image_second',
            'active',
        ]);

        parent::__construct($attributes);
    }

    public function getRouteKeyName() : string
    {
        return 'code';
    }

    public function attributes() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(\Totem\SamProducts\App\Model\Attribute::class, 'attributable', 'products_attributables', 'attributable_code','attribute_id','code','id')->orderBy('order');
    }

    public function variants() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(\Totem\SamProducts\App\Model\Variant::class, 'attributable', 'products_attributables', 'attributable_code','attribute_id','code','id')->orderBy('order');
    }

    public function components() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(\Totem\SamProducts\App\Model\Component::class, 'attributable', 'products_attributables', 'attributable_code','attribute_id','code','id')->orderBy('order');
    }

    public function getImageAttribute($image) : string
    {
        if ($image !== null && is_file('storage/' . $image)) {
            return asset('storage/' . $image);
        }
        return url('images/avatar_2x.png');
    }
}