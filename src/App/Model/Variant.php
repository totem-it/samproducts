<?php

namespace Totem\SamProducts\App\Model;

class Variant extends Attribute
{

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);

        $this->setTable('products_attributes');
        $this->setAttribute('type', __CLASS__);
    }

    protected static function boot() : void
    {
        parent::boot();

        self::globalScope(self::class);
    }

}