<?php

namespace Totem\SamProducts\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;
use Totem\SamProducts\App\Model\Product;

interface ProductRepositoryInterface extends RepositoryInterface
{
    public function calculateForm(string $code);

    public function findByCode(string $code) : Product;

    public function allActive(array $columns = ['*']);

}