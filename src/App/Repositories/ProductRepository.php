<?php

namespace Totem\SamProducts\App\Repositories;

use Totem\SamProducts\App\Model\CalculationForm;
use Totem\SamProducts\App\Model\Product;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamProducts\App\Repositories\Contracts\ProductRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Totem\SamProducts\App\Model\Product model
 */
class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{

    public function model(): string
    {
        return Product::class;
    }

    public function calculateForm(string $code)
    {
        $product = $this->findByCode($code);

        $product = new CalculationForm($product);

        return $product;
    }

    public function findByCode(string $code) : Product
    {
        if ($code === 0) {
            throw new RepositoryException( __('No id have been given.') );
        }

        $data = $this->model->with([
            'attributes',
            'variants',
            'variants.attributes',
            'components',
            'components.attributes',
            'components.variants',
            'components.variants.attributes',
        ])->where('code', '=', $code)->first();

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or element not exist.', ['code' => $code]));
        }

        return $data;
    }

    public function allActive(array $columns = ['*'])
    {
        return $this->model->where('active', 1)->get($columns);
    }
}