<?php

namespace Totem\SamProducts\App\ViewCreators;

use Illuminate\View\View;
use Totem\SamProducts\App\Repositories\Contracts\ProductRepositoryInterface;

class WelcomeCreator
{

    protected $products;

    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;
    }

    public function create(View $view) : void
    {
        $view->with('products', $this->products->allActive());
    }
}