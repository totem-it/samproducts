<?php

namespace Totem\SamProducts;

use Totem\SamAdmin\App\Traits\HasMenu;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamProductsServiceProvider extends ServiceProvider
{
    use TraitServiceProvider, HasMenu;

    public function getNamespace() : string
    {
        return 'sam-products';
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
//        $this->loadViewsFrom(__DIR__ . '/resources/views', $this->getNamespace());
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->extendMenu(config($this->getNamespace().'.menu'));

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
//        $this->publishes([
//            __DIR__ . '/resources/lang' => resource_path('lang/vendor/'.$this->getNamespace()),
//        ], $this->getNamespace().'-lang');
//        $this->publishes([
//            __DIR__ . '/resources/views' => resource_path('views/vendor/'.$this->getNamespace()),
//        ], $this->getNamespace().'-views');
        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ], $this->getNamespace().'-migrations');

        view()->creator('welcome', \Totem\SamProducts\App\ViewCreators\WelcomeCreator::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());
        $this->loadRoutesFrom(__DIR__ . '/route.admin.php');
        $this->loadRoutesFrom(__DIR__ . '/route.web.php');
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->configureBinding([
            \Totem\SamProducts\App\Repositories\Contracts\ProductRepositoryInterface::class => \Totem\SamProducts\App\Repositories\ProductRepository::class,
        ]);


    }

}
