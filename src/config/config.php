<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu
    |--------------------------------------------------------------------------
    |
    | This is the menu array. It is used by SamAdmin Menu Class to create menu list.
    |
    */
    'menu' => [
        'products' => [
//            'key' => 'products.view',
            'key' => null,
            'name' => 'Products',
            'route' => 'products',
            'icon-class' => 'fa-cubes',
            'children' => [
//                [
////                    'key' => 'products.view',
//                    'key' => null,
//                    'name' => 'Customers',
//                    'route' => 'customers.index',
//                    'icon-class' => 'fa-users',
//                    'children' => [],
//                ],
//                [
////                    'key' => 'customer-groups.view',
//                    'key' => null,
//                    'name' => 'Groups',
//                    'route' => 'groups.index',
//                    'icon-class' => 'fa-archive',
//                    'children' => [],
//                ],
            ],
        ],
    ],

];