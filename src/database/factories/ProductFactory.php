<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->define(\Totem\SamProducts\App\Model\Product::class, function (Faker $faker) {

    return [
        'code' => $faker->unique()->domainWord,
        'name' => $faker->sentence,
        'description' => $faker->text,
        'keywords' => implode(';', $faker->words),
        'image' => 'products/'. $faker->image(public_path('storage/products'), 350, 200, null, false),
        'image_second' => 'products/'. $faker->image(public_path('storage/products'), 350, 200, null, false),
        'active' => $faker->numberBetween(0, 1),
    ];
});
