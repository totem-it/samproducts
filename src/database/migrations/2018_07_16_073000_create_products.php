<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('code')->unique();
                $table->string('name');
                $table->text('description')->nullable();
                $table->text('keywords')->nullable();
                $table->string('image')->nullable();
                $table->string('image_second')->nullable();
                $table->tinyInteger('active')->default(1);
                $table->softDeletes();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamProducts\Database\Seeds\ProductsSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('products');
    }
}
