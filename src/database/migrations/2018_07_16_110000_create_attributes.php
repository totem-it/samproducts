<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create('products_attributes', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->timestamps();

                $table->string('code');
                $table->string('name');

                $table->string('type');
                $table->string('field_type');

                $table->string('suffix')->nullable();

                $table->text('description')->nullable();
                $table->string('default')->nullable();
                $table->integer('step')->nullable();
                $table->integer('min')->nullable();
                $table->integer('max')->nullable();

                $table->integer('order')->default(0);

                $table->softDeletes();
            });

            Schema::create('products_attributables', function (Blueprint $table) {
                $table->string('attribute_id');
                $table->string('attributable_code');
                $table->string('attributable_type');

                $table->foreign('attribute_id')->references('id')->on('products_attributes')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['attribute_id']);

            });

            Artisan::call('db:seed', [
                '--class' => \Totem\SamProducts\Database\Seeds\Products\HardCoverSewnSeeder::class,
            ]);

            Artisan::call('db:seed', [
                '--class' => \Totem\SamProducts\Database\Seeds\Products\SoftCoverGluedSeeder::class,
            ]);

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('products_attributables');
        Schema::dropIfExists('products_attributes');
    }
}
