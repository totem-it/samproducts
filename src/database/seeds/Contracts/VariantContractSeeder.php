<?php

namespace Totem\SamProducts\Database\Seeds\Contracts;

use Totem\SamProducts\App\Model\Attribute;
use Totem\SamProducts\App\Model\Variant;

abstract  class VariantContractSeeder
{
    /**
     *  return [
     *      [
     *          'code' => 'unique_code',
     *          'name' => 'translatable_name',
     *          'default' => '1_or_null',
     *          'order' => 'integer',
     *      ],
     *  ]
     */
    abstract public static function setOptions() : array;

    abstract public static function variant(int $order = 0, array $options = []) : Variant;

    public static function getOptions() : array
    {
        return array_map(function($item){
            $item['field_type'] =  \Totem\SamProducts\App\Model\Fields\Option::class;
            return new Attribute($item);
        }, static::setOptions());

    }

    public static function saveMany(\Illuminate\Database\Eloquent\Relations\MorphToMany $relation, array $options = []) : void
    {
        $relation->saveMany(
            empty($options) ? self::getOptions() : $options
        );
    }
}