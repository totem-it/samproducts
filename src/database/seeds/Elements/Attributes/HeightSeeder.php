<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class HeightSeeder
{

    public static function attribute(int $order = 0, float $default = 148) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputNumber::class,
            'code' => 'height',
            'name' => 'Height',
            'suffix' => 'mm',
            'default' => $default,
            'order' => $order,
        ]);
    }

}