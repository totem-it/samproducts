<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class InsertSeeder
{

    public static function attribute(int $order = 0, float $default = null) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Checkbox::class,
            'code' => 'insert',
            'name' => 'Insert',
            'default' => $default,
            'order' => $order,
        ]);
    }

}