<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class IsbnSeeder
{

    public static function attribute(int $order = 0) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputText::class,
            'code' => 'isbn',
            'name' => 'ISBN',
            'order' => $order,
        ]);
    }

}