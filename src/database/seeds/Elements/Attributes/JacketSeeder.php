<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class JacketSeeder
{

    public static function attribute(int $order = 0, int $default = null) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Checkbox::class,
            'code' => 'jacket',
            'name' => 'Dust jacket',
            'default' => $default,
            'order' => $order,
        ]);
    }

}