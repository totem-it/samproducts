<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class PageCountSeeder
{

    public static function attribute(int $order = 0, float $default = 140, string $code = 'pages_count', string $name = 'Number of pages') : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputNumber::class,
            'code' => $code,
            'name' => $name,
            'default' => $default,
            'order' => $order,
        ]);
    }

}