<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class PrintRunSeeder
{

    public static function attribute(int $order = 0) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputNumber::class,
            'code' => 'qty',
            'name' => 'Print run',
            'suffix' => 'units',
            'default' => 100,
            'order' => $order,
        ]);
    }

}