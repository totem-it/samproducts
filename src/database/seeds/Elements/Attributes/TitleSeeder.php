<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class TitleSeeder
{

    public static function attribute(int $order = 0) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputText::class,
            'code' => 'title',
            'name' => 'Title',
            'order' => $order,
        ]);
    }

}