<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Attributes;

use Totem\SamProducts\App\Model\Attribute;

class WidthSeeder
{

    public static function attribute(int $order = 0, float $default = 105) : Attribute
    {
        return Attribute::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputNumber::class,
            'code' => 'width',
            'name' => 'Width',
            'suffix' => 'mm',
            'default' => $default,
            'order' => $order,
        ]);
    }

}