<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Attribute;
use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class BookmarkSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'jdr_901',
                'name' => 'white (JDR no 901)',
                'default' => null,
                'order' => 1,
            ],
            [
                'code' => 'jdr_931',
                'name' => 'black (JDR no 931)',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'jdr_903',
                'name' => 'cream (JDR no 903)',
                'default' => null,
                'order' => 3,
            ],
            [
                'code' => 'jdr_916',
                'name' => 'red (JDR no 916)',
                'default' => null,
                'order' => 4,
            ],
            [
                'code' => 'jdr_930',
                'name' => 'green (JDR no 930)',
                'default' => null,
                'order' => 5,
            ],
            [
                'code' => 'jdr_923',
                'name' => 'dark blue (JDR no 923)',
                'default' => null,
                'order' => 6,
            ],
            [
                'code' => 'jdr_908',
                'name' => 'gold (JDR no 908)',
                'default' => null,
                'order' => 7,
            ],
            [
                'code' => 'jdr_912',
                'name' => 'silver (JDR no 912)',
                'default' => null,
                'order' => 8,
            ],
            [
                'code' => 'bookmark_other',
                'name' => 'other',
                'default' => null,
                'order' => 9,
            ],
            [
                'code' => 'bookmark_none',
                'name' => 'none',
                'default' => 1,
                'order' => 10,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'bookmark',
            'name' => 'Bookmark ribbon',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

    /**
     * Run the database seeds.
     *
     * @param int $order
     * @return Attribute
     */
    public static function attribute_other(int $order = 0) : Attribute
    {
        return new Attribute([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputText::class,
            'code' => 'bookmark_other',
            'name' => 'Enter bookmark ribbon color',
            'description' => '<p>Check the available colors on the manufacturer\'s website and enter the color code in the box below.</p>',
            'default' => null,
            'order' => $order,
        ]);
    }

}