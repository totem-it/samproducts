<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class CaseWrapSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'canvas',
                'name' => 'Canvas',
                'default' => null,
                'order' => 1,
            ],
            [
                'code' => 'paper',
                'name' => 'Paper',
                'default' => 1,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'material',
            'name' => 'Material',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}