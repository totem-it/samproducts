<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Components;

use Totem\SamProducts\App\Model\Component;

class BandSeeder
{

    public static function attribute(int $order = 0) : Component
    {
        return Component::create([
            'field_type' => \Totem\SamProducts\App\Model\Component::class,
            'code' => 'band',
            'name' => 'Band',
            'order' => $order,
        ]);
    }

}