<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Components;

use Totem\SamProducts\App\Model\Component;

class CaseBoxSeeder
{

    public static function attribute(int $order = 0) : Component
    {
        return Component::create([
            'field_type' => \Totem\SamProducts\App\Model\Component::class,
            'code' => 'case_box',
            'name' => 'Case box',
            'order' => $order,
        ]);
    }

}