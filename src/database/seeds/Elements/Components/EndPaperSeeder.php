<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Components;

use Totem\SamProducts\App\Model\Component;

class EndPaperSeeder
{

    public static function attribute(int $order = 0) : Component
    {
        return Component::create([
            'field_type' => \Totem\SamProducts\App\Model\Component::class,
            'code' => 'endpapers',
            'name' => 'End paper',
            'order' => $order,
        ]);
    }

}