<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Components;

use Totem\SamProducts\App\Model\Component;

class InsideSeeder
{

    public static function attribute(int $order = 0) : Component
    {
        return Component::create([
            'field_type' => \Totem\SamProducts\App\Model\Component::class,
            'code' => 'inside',
            'name' => 'Inside',
            'order' => $order,
        ]);
    }

}