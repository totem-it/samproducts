<?php

namespace Totem\SamProducts\Database\Seeds\Elements\Components;

use Totem\SamProducts\App\Model\Component;

class JacketSeeder
{

    public static function attribute(int $order = 0) : Component
    {
        return Component::create([
            'field_type' => \Totem\SamProducts\App\Model\Component::class,
            'code' => 'jacket',
            'name' => 'Dust Jacket',
            'order' => $order,
        ]);
    }

}