<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class CoverCardboardSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'cardboard_2_5',
                'name' => '2.5',
                'default' => 1,
                'order' => 3,
            ],
            [
                'code' => 'cardboard_1_5',
                'name' => '1.5',
                'default' => null,
                'order' => 1,
            ],
            [
                'code' => 'cardboard_2_0',
                'name' => '2.0',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'cardboard_3_0',
                'name' => '3.0',
                'default' => null,
                'order' => 4,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'cardboard',
            'name' => 'Cardboard thickness',
            'suffix' => 'mm',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}