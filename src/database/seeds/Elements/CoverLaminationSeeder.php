<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class CoverLaminationSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'glossy_lamination',
                'name' => 'Glossy',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'matt_lamination',
                'name' => 'Matt',
                'default' => null,
                'order' => 3,
            ],
            [
                'code' => 'soft_touch_lamination',
                'name' => 'Soft-touch',
                'default' => null,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'cover_lamination',
            'name' => 'Lamination by foil',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}