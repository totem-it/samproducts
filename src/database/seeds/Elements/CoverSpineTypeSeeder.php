<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class CoverSpineTypeSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'flat',
                'name' => 'Flat',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'round',
                'name' => 'Round',
                'default' => null,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'back_type',
            'name' => 'Back type',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}