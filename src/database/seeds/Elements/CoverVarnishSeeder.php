<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class CoverVarnishSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'full_surface_varnish',
                'name' => 'Full surface varnish',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'spot_varnish',
                'name' => 'Spot varnish',
                'default' => null,
                'order' => 3,
            ],
            [
                'code' => 'cover_varnish_none',
                'name' => 'none',
                'default' => 1,
                'order' => 1,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'cover_varnish',
            'name' => 'Cover varnish',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}