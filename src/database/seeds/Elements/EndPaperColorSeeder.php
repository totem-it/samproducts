<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class EndPaperColorSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'endpapers_mono',
                'name' => 'black and white',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'endpapers_color',
                'name' => 'color',
                'default' => null,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'endpapers_color_type',
            'name' => 'Endpapers color',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }


}