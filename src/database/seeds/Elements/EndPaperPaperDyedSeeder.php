<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class EndPaperPaperDyedSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'rainbow_03',
                'name' => 'Creme (Rainbow 03)',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'rainbow_06',
                'name' => 'Chamois (Rainbow 06)',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'rainbow_12',
                'name' => 'Light Yellow (Rainbow 12)',
                'default' => null,
                'order' => 3,
            ],
            [
                'code' => 'rainbow_14',
                'name' => 'Medium Yellow (Rainbow 14)',
                'default' => null,
                'order' => 4,
            ],
            [
                'code' => 'rainbow_22',
                'name' => 'Medium Orange (Rainbow 22)',
                'default' => null,
                'order' => 5,
            ],
            [
                'code' => 'rainbow_40',
                'name' => 'Salmon (Rainbow 40)',
                'default' => null,
                'order' => 6,
            ],
            [
                'code' => 'rainbow_54',
                'name' => 'Light Pink (Rainbow 54)',
                'default' => null,
                'order' => 7,
            ],
            [
                'code' => 'rainbow_55',
                'name' => 'Pink (Rainbow 55)',
                'default' => null,
                'order' => 8,
            ],
            [
                'code' => 'rainbow_60',
                'name' => 'Violet (Rainbow 60)',
                'default' => null,
                'order' => 9,
            ],
            [
                'code' => 'rainbow_72',
                'name' => 'Light Green (Rainbow 72)',
                'default' => null,
                'order' => 10,
            ],
            [
                'code' => 'rainbow_75',
                'name' => 'Medium Green (Rainbow 75)',
                'default' => null,
                'order' => 11,
            ],
            [
                'code' => 'rainbow75_',
                'name' => 'Light Blue (Rainbow 82)',
                'default' => null,
                'order' => 12,
            ],
            [
                'code' => 'rainbow_84',
                'name' => 'Medium Blue (Rainbow 84)',
                'default' => null,
                'order' => 13,
            ],
            [
                'code' => 'rainbow_93',
                'name' => 'Light Grey (Rainbow 93)',
                'default' => null,
                'order' => 14,
            ],
            [
                'code' => 'rainbow_96',
                'name' => 'Grey (Rainbow 96)',
                'default' => null,
                'order' => 15,
            ],
            [
                'code' => 'rainbow_16',
                'name' => 'Yellow (Rainbow 16)',
                'default' => null,
                'order' => 16,
            ],
            [
                'code' => 'rainbow_18',
                'name' => 'Intensive Yellow (Rainbow 18)',
                'default' => null,
                'order' => 17,
            ],
            [
                'code' => 'rainbow_24',
                'name' => 'Orange (Rainbow 24)',
                'default' => null,
                'order' => 18,
            ],
            [
                'code' => 'rainbow_26',
                'name' => 'Intensive Orange (Rainbow 26)',
                'default' => null,
                'order' => 19,
            ],
            [
                'code' => 'rainbow_28',
                'name' => 'Intensive Red (Rainbow 28)',
                'default' => null,
                'order' => 20,
            ],
            [
                'code' => 'rainbow_74',
                'name' => 'Bright Green (Rainbow 74)',
                'default' => null,
                'order' => 21,
            ],
            [
                'code' => 'rainbow_76',
                'name' => 'Green (Rainbow 76)',
                'default' => null,
                'order' => 22,
            ],
            [
                'code' => 'rainbow_78',
                'name' => 'Intensive Green (Rainbow 78)',
                'default' => null,
                'order' => 23,
            ],
            [
                'code' => 'rainbow_87',
                'name' => 'Blue (Rainbow 87)',
                'default' => null,
                'order' => 24,
            ],
            [
                'code' => 'rainbow_88',
                'name' => 'Intensive Blue (Rainbow 88)',
                'default' => null,
                'order' => 25,
            ],
            [
                'code' => 'rainbow_99',
                'name' => 'Black (Rainbow 99)',
                'default' => null,
                'order' => 26,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'endpapers_dyed_color',
            'name' => 'Choose dyed paper',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }


}