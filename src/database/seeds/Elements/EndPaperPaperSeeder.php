<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class EndPaperPaperSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'endpapers_white',
                'name' => 'wood free offset, 140 gsm',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'endpapers_creamy',
                'name' => 'creamy paper, 130 gsm',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'endpapers_dyed',
                'name' => 'dyed paper, 160 gsm',
                'default' => null,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'endpapers_paper',
            'name' => 'Endpapers material',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }


}