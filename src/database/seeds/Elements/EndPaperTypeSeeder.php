<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class EndPaperTypeSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'endpapers_type_none',
                'name' => 'unprinted',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'endpapers_type_one',
                'name' => 'one-side printed',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'endpapers_type_two',
                'name' => 'two-side printed',
                'default' => null,
                'order' => 3,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'endpapers_type',
            'name' => 'Endpapers',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }


}