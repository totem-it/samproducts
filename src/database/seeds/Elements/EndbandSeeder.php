<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Attribute;
use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class EndbandSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'jdr_101',
                'name' => 'white (JDR no 101)',
                'default' => null,
                'order' => 1,
            ],
            [
                'code' => 'jdr_529',
                'name' => 'black (JDR no 529)',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'jdr_102',
                'name' => 'cream (JDR no 102)',
                'default' => null,
                'order' => 3,
            ],
            [
                'code' => 'jdr_105',
                'name' => 'red (JDR no 105)',
                'default' => null,
                'order' => 4,
            ],
            [
                'code' => 'jdr_109',
                'name' => 'green (JDR no 109)',
                'default' => null,
                'order' => 5,
            ],
            [
                'code' => 'jdr_110',
                'name' => 'dark blue (JDR no 110)',
                'default' => null,
                'order' => 6,
            ],
            [
                'code' => 'jdr_104',
                'name' => 'gold (JDR no 104)',
                'default' => null,
                'order' => 7,
            ],
            [
                'code' => 'jdr_107',
                'name' => 'silver (JDR no 107)',
                'default' => null,
                'order' => 8,
            ],
            [
                'code' => 'endband_other',
                'name' => 'other',
                'default' => null,
                'order' => 9,
            ],
            [
                'code' => 'endband_none',
                'name' => 'none',
                'default' => 1,
                'order' => 10,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'endband',
            'name' => 'Endband',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

    public static function attribute_other(int $order = 0) : Attribute
    {
        return new Attribute([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputText::class,
            'code' => 'endband_other',
            'name' => 'Type endband color',
            'description' => '<p>Check the available colors on the manufacturer\'s website and enter the color code in the box below.</p>',
            'default' => null,
            'order' => $order,
        ]);
    }

}