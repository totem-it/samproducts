<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Attribute;
use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class HotStampingSeeder extends VariantContractSeeder
{

    public static function attribute_pressure_range(int $order = 0) : Attribute
    {
        return new Attribute([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Range::class,
            'code' => 'hot_stamping_pressure',
            'name' => 'Pressure cm²',
            'default' => 100,
            'step' => 100,
            'min' => 100,
            'max' => 300,
            'order' => $order,
        ]);
    }

    public static function attribute_color_other(int $order = 0) : Attribute
    {
        return new Attribute([
            'field_type' => \Totem\SamProducts\App\Model\Fields\InputText::class,
            'code' => 'hot_stamping_other',
            'name' => 'Enter hot-stamping color',
            'description' => '<p>Please enter color proposition in box. We will check it availability.</p>',
            'default' => null,
            'order' => $order,
        ]);
    }

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'hot_stamping_none',
                'name' => 'none',
                'default' => 1,
                'order' => 4,
            ],
            [
                'code' => 'hot_stamping_gold',
                'name' => 'gold',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'hot_stamping_silver',
                'name' => 'silver',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'hot_stamping_other',
                'name' => 'other',
                'default' => null,
                'order' => 3,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'hot_stamping',
            'name' => 'Hot-stamping',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}