<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;

class PaperSeeder
{

    public static function variant_type(int $order = 0, string $code = 'paper_type', string $name = 'Paper type') : Variant
    {
        return Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => $code,
            'name' => $name,
            'order' => $order,
        ]);
    }

    public static function variant_weights(int $order = 0, string $code = 'paper_weight', string $name = 'Paper weight') : Variant
    {
        return Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => $code,
            'name' => $name,
            'suffix' => 'g/m²',
            'order' => $order,
        ]);
    }

}