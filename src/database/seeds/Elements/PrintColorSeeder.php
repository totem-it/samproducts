<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class PrintColorSeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'color_mono',
                'name' => '1 + 1',
                'default' => 1,
                'order' => 1,
            ],
            [
                'code' => 'color_color',
                'name' => '4 + 4',
                'default' => null,
                'order' => 2,
            ],
            [
                'code' => 'color_mixed',
                'name' => 'mixed',
                'default' => null,
                'order' => 3,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'print_color',
            'name' => 'Color',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }


}