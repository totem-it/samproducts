<?php

namespace Totem\SamProducts\Database\Seeds\Elements;

use Totem\SamProducts\App\Model\Variant;
use Totem\SamProducts\Database\Seeds\Contracts\VariantContractSeeder;

class PrintTechnologySeeder extends VariantContractSeeder
{

    public static function setOptions() : array
    {
        return [
            [
                'code' => 'print_technology_toner',
                'name' => 'Toner print',
                'description' => 'The most optimal and efficient digital print technique, perfect for both simple text books, and for complex ones with large amount of graphical elements, providing uniform coor reproduction.',
                'default' => null,
                'order' => 1,
            ],
            [
                'code' => 'print_technology_inkjet',
                'name' => 'Ink-jet print',
                'description' => 'Printing technique, provides good-quality print jobs with graphics of varying difficulty, including photos, illustrations with lots of details, and large-area printers. The technique can be used on all types of paper used in books. Economical even from several copies.',
                'default' => 1,
                'order' => 2,
            ],
        ];
    }

    public static function variant(int $order = 0, array $options = []) : Variant
    {
        $variant = Variant::create([
            'field_type' => \Totem\SamProducts\App\Model\Fields\Select::class,
            'code' => 'print_technology',
            'name' => 'Print technology',
            'order' => $order,
        ]);

        self::saveMany($variant->attributes(), $options);

        return $variant;
    }

}