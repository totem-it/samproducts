<?php

namespace Totem\SamProducts\Database\Seeds\Products;

use Totem\SamProducts\App\Model\Product;
use Totem\SamProducts\Database\Seeds\Elements;
use Totem\SamProducts\Database\Seeds\Elements\Components;
use Totem\SamProducts\Database\Seeds\Elements\Attributes;
use Illuminate\Database\Seeder;

class HardCoverSewnSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $product = Product::where('code', '=', 'hard-cover-sewn')->first();

        if (empty($product)) {
            return;
        }

        $product->components()->saveMany([
            Components\InsideSeeder::attribute(1),
            Components\CoverSeeder::attribute(2),
            Components\JacketSeeder::attribute(3),
            Components\EndPaperSeeder::attribute(4),
//            Components\BandSeeder::attribute(5),
//            Components\PocketSeeder::attribute(6),
//            Components\CaseBoxSeeder::attribute(7),
        ]);

        $product->attributes()->saveMany([
            Attributes\TitleSeeder::attribute(1),
            Attributes\IsbnSeeder::attribute(2),
            Attributes\PrintRunSeeder::attribute(3),
            Attributes\WidthSeeder::attribute(4),
            Attributes\HeightSeeder::attribute(5),
            Elements\EndbandSeeder::attribute_other(7),
            Elements\BookmarkSeeder::attribute_other(9),
        ]);

        $product->variants()->saveMany([
            Elements\EndbandSeeder::variant(6),
            Elements\BookmarkSeeder::variant(8),
        ]);

        /** @var \Totem\SamProducts\App\Model\Component $inside */
        $inside = $product->components->firstWhere('code', 'inside');
        if ($inside !== null) {

            $inside->attributes()->saveMany([
                Attributes\InsertSeeder::attribute(6),
                Attributes\PageCountSeeder::attribute(3),
                Attributes\PageCountSeeder::attribute(7, 0, 'pages_insert_count'),
            ]);

            $inside->variants()->saveMany([
                Elements\PrintTechnologySeeder::variant(1),
                Elements\PrintColorSeeder::variant(2),
                Elements\PaperSeeder::variant_type(4),
                Elements\PaperSeeder::variant_weights(5),
                Elements\PaperSeeder::variant_type(8, 'paper_insert_type'),
                Elements\PaperSeeder::variant_weights(9, 'paper_insert_weight'),
            ]);
        }

        /** @var \Totem\SamProducts\App\Model\Component $cover */
        $cover = $product->components->firstWhere('code', 'cover');
        if ($cover !== null) {

            $cover->attributes()->saveMany([
                Elements\HotStampingSeeder::attribute_pressure_range(7),
                Elements\HotStampingSeeder::attribute_color_other(8),
            ]);

            $cover->variants()->saveMany([
                Elements\CoverSpineTypeSeeder::variant(1),
                Elements\CoverCardboardSeeder::variant(2),
                Elements\CaseWrapSeeder::variant(3),
                Elements\CoverLaminationSeeder::variant(4),
                Elements\CoverVarnishSeeder::variant(5),
                Elements\HotStampingSeeder::variant(6),
            ]);
        }

        /** @var \Totem\SamProducts\App\Model\Component $jacket */
        $jacket = $product->components->firstWhere('code', 'jacket');

        if ($jacket !== null) {
            $jacket->attributes()->saveMany([
                Attributes\JacketSeeder::attribute(1),
                Elements\HotStampingSeeder::attribute_pressure_range(5),
                Elements\HotStampingSeeder::attribute_color_other(6),
            ]);

            $jacket->variants()->saveMany([
                Elements\CoverLaminationSeeder::variant(2),
                Elements\CoverVarnishSeeder::variant(3),
                Elements\HotStampingSeeder::variant(4),
            ]);
        }

        /** @var \Totem\SamProducts\App\Model\Component $endpapers */
        $endpapers = $product->components->firstWhere('code', 'endpapers');

        if ($endpapers !== null) {
            $endpapers->variants()->saveMany([
                Elements\EndPaperTypeSeeder::variant(1),
                Elements\EndPaperColorSeeder::variant(2),
                Elements\EndPaperPaperSeeder::variant(3),
                Elements\EndPaperPaperDyedSeeder::variant(4),
            ]);
        }
    }

}