<?php

namespace Totem\SamProducts\Database\Seeds\Products;

use Totem\SamProducts\App\Model\Product;
use Totem\SamProducts\Database\Seeds\Elements;
use Totem\SamProducts\Database\Seeds\Elements\Components;
use Totem\SamProducts\Database\Seeds\Elements\Attributes;
use Illuminate\Database\Seeder;

class SoftCoverGluedSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $product = Product::where('code', '=', 'soft-cover-glued')->first();

        if (!empty($product)) {

            $product->components()->saveMany([
                Components\InsideSeeder::attribute(1),
                Components\CoverSeeder::attribute(2),
                Components\JacketSeeder::attribute(3),
            ]);

            $product->attributes()->saveMany([
                Attributes\TitleSeeder::attribute(1),
                Attributes\IsbnSeeder::attribute(2),
                Attributes\PrintRunSeeder::attribute(3),
                Attributes\WidthSeeder::attribute(4),
                Attributes\HeightSeeder::attribute(5),
            ]);

            /** @var \Totem\SamProducts\App\Model\Component $inside */
            $inside = $product->components->firstWhere('code', 'inside');
            if ($inside !== null) {

                $inside->attributes()->saveMany([
                    Attributes\InsertSeeder::attribute(6),
                    Attributes\PageCountSeeder::attribute(3),
                    Attributes\PageCountSeeder::attribute(7, 0, 'pages_insert_count'),
                ]);

                $inside->variants()->saveMany([
                    Elements\PrintTechnologySeeder::variant(1),
                    Elements\PrintColorSeeder::variant(2),
                    Elements\PaperSeeder::variant_type(4),
                    Elements\PaperSeeder::variant_weights(5),
                    Elements\PaperSeeder::variant_type(8, 'paper_insert_type'),
                    Elements\PaperSeeder::variant_weights(9, 'paper_insert_weight'),
                ]);
            }

        }

    }

}