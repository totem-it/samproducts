<?php

namespace Totem\SamProducts\Database\Seeds;

use Totem\SamProducts\App\Model\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {

        Product::create([
            'code' => 'hard-cover-sewn',
            'name' => 'Hardback – thread sewn',
            'description' => 'Hard-wearing and the most advanced form of binding that guarantees durability for many years of use. Ideal for high-class and select publications. In particular used where it is required for the interior to open fully (lay flat). The sections here are bound with sturdy hemp thread.',
            'keywords' => 'hardcover;sewn;hardback;thread;',
            'image' => 'products/twarda-szyta.png',
            'image_second' => 'products/1-detal-twarda-szyta_1.png',
        ]);

        Product::create([
            'code' => 'soft-cover-glued',
            'name' => 'Perfect bound',
            'description' => null,
            'keywords' => 'perfect;bound;soft;',
            'image' => 'products/miekka-klejona.png',
            'image_second' => 'products/3-detal-miekka-klejona.png',
        ]);

        if (config('app.env') !== 'production') {
            factory(Product::class, 15)->create();
        }
    }
}
