<?php

Route::group(['middleware' => ['web']], function () {

    Route::group(['prefix' => 'product'], function() {

        Route::get('{product}', 'Totem\SamProducts\App\Controllers\ProductsController@calculate')->name('products.calculate');

    });

});